import React from 'react';
import {NavLink} from 'react-router-dom';

const Navigation = () => {
    return (
    <div className ="sidebar">
        <div className="id">
           <div className="idContent">
             <img src="./media/rawaa.png" alt="the profile pic" width="150" height="150"/>
             <h3>Rawaa KECHICHE</h3>
             </div>   
        </div>

        <div className="navigation">
        <ul>
            <li>
                <NavLink exact to="/"activeClassName="navActive">
                    <i className="fas fa-home"></i>
                    <span>Acceuil</span>
                </NavLink>
            </li>
            <li>
                <NavLink exact to="/competences"activeClassName="navActive">
                    <i className="fas fa-mountain"></i>
                    <span>Compétences</span>
                </NavLink>
            </li>
            <li>
                <NavLink exact to="/portfolio"activeClassName="navActive">
                    <i className="fas fa-images"></i>
                    <span>Portfolio</span>
                </NavLink>
            </li>
            <li>
                <NavLink exact to="/contact"activeClassName="navActive">
                    <i className="fas fa-address-book"></i>
                    <span>Contact</span>
                </NavLink>
            </li>
        </ul>

             </div>
            <div className="socialNetwork">
             <ul>
                 <li>
                     <a href ="https://www.linkedin.com/in/rawaa-kechiche-450729201/" target="blank"rel="noopener noreferrer"><i className="fab fa-linkedin"></i></a>
                 </li>
                 <li>
                     <a href ="https://www.instagram.com/kechiche.raoua/" target="blank"rel="noopener noreferrer"><i className="fab fa-instagram"></i></a>
                 </li>
                 <li>
                     <a href ="https://gitlab.com/Rawaa_kech" target="blank"rel="noopener noreferrer"><i className="fab fa-gitlab"></i></a>
                 </li>
             </ul>
            <div className="signature">
               <p>-Rawaa KECHICHE-</p>
            </div>
            </div>

    </div>
    );
};

export default Navigation;