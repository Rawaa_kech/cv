import React from 'react';

const Formation = () => {
    return (
        <div className="formation">
            <h3>Formation</h3>
            <div className="f-1">
                <h4>Licence informatique 2019-2022</h4>
                <h5>Université Paris Saclay (2021-2022)</h5>
                <p>Licence MIAGE</p>
                <h5>Université de lille (2019-2021)</h5>
                <p>1<sup>ére</sup> année option Mathematiques-Informatique</p>
                <p>2<sup>éme</sup> année informatique</p>
                <h4>Baccalauréat Mathematiques,Tunisie 2018-2019</h4>
                <h5>Lycée 2 Mars 1934 Mention assez-bien</h5>

            </div>
            <div className="f-2">
            <h3>Expérience professionelle</h3>
            <h4>Juin-Août 2020 : MediaMarket
            </h4>
            <h5>Employée de magasin,Mulhouse</h5>
            <ul>
            <li>
                Vérifier la qualité,l'étiquetage de la marchandise receptionnée;
            </li>
            <li>
                Ranger la marchandise;
            </li>
            <li>
                Réaliser des comptages pour recaler le stock;
            </li>
            <li>
                Assurer l'encaissement;
            </li>
            </ul>
            </div>
            

        </div>
    );
};

export default Formation;