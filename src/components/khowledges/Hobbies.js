import React from 'react';

const Hobbies = () => {
    return (
        <div className="hobbies">
            <h3>Intérêts</h3>
            <ul>
                <li className="hobby">
                <i class="fas fa-running"></i>                   
                 <span>Fitness/cardio</span>
                </li>
                <li className="hobby">
                <i class="fas fa-cookie-bite"></i>
                                                 <span>Cuisine</span>
                </li>
                <li className="hobby">
                <i class="fas fa-shopping-bag"></i>           
                      <span>La mode</span>
                </li>
                
            </ul>
            
        </div>
    );
};

export default Hobbies;