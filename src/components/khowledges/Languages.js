import React, { Component } from 'react';
import ProgressBar from './ProgressBar';
class Languages extends Component {
   state={
       technical:[
           {id:1,value:"Html/Css",xp:2},
           {id:2,value:"Javascript",xp:2},
           {id:3,value:"Gitlab",xp:2},
           {id:4,value:"Php",xp:1},
           {id:5,value:"Python",xp:1},
           {id:6,value:"Pascal",xp:2},
           {id:7,value:"Java",xp:1},
           {id:8,value:"C",xp:1},
           {id:9,value:"React",xp:1},
           {id:10,value:"MySQL/PostgreSQL",xp:1},
           {id:11,value:"Node.js",xp:1},
           {id:12,value:"Concéption",xp:1},
           {id:13,value:"VS Code",xp:2},
           {id:14,value:"Pack Office",xp:2}



       ]
   }

    render() {
          let {technical}=this.state;
        return (
            <div className="technicalCmp">
                <ProgressBar
                 technical={technical}
                 className="technicalDisplay"
                 title="compétences techniques"

                 />
            </div>
        );
    }
}

export default Languages;