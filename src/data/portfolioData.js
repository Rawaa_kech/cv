export const portfolioData = [
  {
    id: 1,
    name: 'Game',
    languages: ['java','database'],
    languagesIcons: ['fab fa-java','fas fa-database'],
    source: 'https://gitlab.com/rawaa-projects2021/game',
    info: 'Ce projet consiste en la modélisation d\'un jeu, cette modélisation permettra d\'implanter différents jeux.Les jeux ont une base commune mais les règles, les types de personnages, la composition du terrain de jeu pourra varier.L\'objectif est donc d\'obtenir une modélisation qui permet à un développeur de créer un nouveau jeu en se servant de ce qui est déja réalisé ou en ajoutant modérément des classes pour s\'adapter aux spécifités de son jeu ',
    picture: './media/project1.png',
    demo:'https://www.youtube.com/watch?v=Hii_NwrYVGA'
  },
  {
    id: 2,
    name: 'Converter',
    languages: ['javascript','node','react'],
    languagesIcons: ['fab fa-js','fab fa-node-js','fab fa-react'],
    source: 'https://gitlab.com/Rawaa_kech/converter',
    info: 'Une  application qui permet de calculer la valeur de change \'un montant en € en plusieurs monnaies.',
    picture: './media/project2.png',
    demo:'https://rawaa-kechiche.000webhostapp.com/divises/'
  },
  {
    id: 3,
    name: 'Starship',
    languages: ['javascript','node','css'],
    languagesIcons: ['fab fa-js','fab fa-node-js','fab fa-css3-alt'],
    source: 'https://gitlab.com/Rawaa_kech/starship',
    info: 'L\'objectif est de réaliser un jeu vidéo simple dans lequel le joueur contrôle à l\'aide du clavier le déplacement vertical d\'un vaisseau situé sur la gauche de l\'écran. Des soucoupes volantes arrivent de la droite de l\'écran et le joueur doit les détruire en leur tirant dessus. A chaque tir réussi le joueur marque des points, à l\'inverse si un vaisseau parvient à passer sans être détruit, le joueur perd des points.',
    picture: './media/project3.png',
    demo:'https://rawaa-kechiche.000webhostapp.com/starship/'
  },
  {
    id: 4,
    name: 'My CV',
    languages: ['javascript','css','react'],
    languagesIcons: ['fab fa-js','fab fa-css3-alt','fab fa-react'],
    source: 'https://gitlab.com/Rawaa_kech/cv',
    info: 'Ce projet est personnel , il represente mon Curriculum Vitaie en format eléctronique.',
    picture: './media/project4.png',
    demo:''
  },
  {
    id: 5,
    name: 'Task manager',
    languages: ['javascript','css','react'],
    languagesIcons: ['fab fa-js', 'fab fa-css3-alt','fab fa-react'],
    source: 'https://gitlab.com/Rawaa_kech/todo',
    info: 'L\'application permet la gestion de tâches définies par une durée et auxquelles il est possible d\'attribuer une priorité. On distingue les tâches à réaliser de celles déjà terminées.',
    picture: './media/project5.png',
    demo:'https://rawaa-kechiche.000webhostapp.com/todo/'
  },
  {
    id: 6,
    name: 'Identification',
    languages: ['php','database'],
    languagesIcons: ['fab fa-php','fas fa-database'],
    source: 'https://gitlab.com/Rawaa_kech/identification',
    info: 'Construction d\'une page web dynamique utilisant des services web fournissant une réponse en JSON.',
    picture: './media/project6.png',
    demo:'https://www.youtube.com/watch?v=ag7z9_Z3Hjc&t=100s'
  },
]