import React from 'react';
import Navigation from '../components/Navigation';
import {CopyToClipboard} from 'react-copy-to-clipboard'

const Contact = () => {
    return (
        <div className="contact">
           <Navigation/>
           <div className="contactContent">
               <div className="header">
               <div className="contactBox">
                   <h1>Contactez-moi</h1>
                   <ul>
                       <li>
                           <i className="fas fa-map-marker-alt"></i>
                           <span>4 rue Fragonard,Saint Maurice</span>
                       </li>
                       <li>
                           <i className="fas fa-mobile-alt"></i>
                           <CopyToClipboard text="0683557173">
                               <span className="clickInput" 
                               onClick={()=>{alert('Teléphone copié !');}}>
                                   06 83 55 71 73
                                   </span>
                           </CopyToClipboard>
                       </li>
                       <li>
                           <i className="fas fa-envelope"></i>
                           <CopyToClipboard text="rawaa.kechiche@gmail.com">
                               <span className="clickInput" 
                               onClick={()=>{alert('E-mail copié !');}}>
                                   rawaa.kechiche@gmail.com
                                   </span>
                           </CopyToClipboard>
                       </li>
                   </ul>

               </div>
               </div>
               <div className="socialNetwork">
                   <ul>
                   <a href ="https://www.linkedin.com/in/rawaa-kechiche-450729201/" target="_blank"rel="noopener noreferrer">
                       <h4>linkedIn</h4>
                       <i className="fab fa-linkedin"></i>
                       </a>  
                       <a href ="https://gitlab.com/Rawaa_kech" target="_blank"rel="noopener noreferrer">
                       <h4>Gitlab</h4>
                       <i className="fab fa-gitlab"></i>
                       </a>  
                       <a href ="https://www.instagram.com/kechiche.raoua/" target="_blank"rel="noopener noreferrer">
                       <h4>Instagram</h4>
                       <i className="fab fa-instagram"></i>
                       </a>  
                   </ul>
               </div>
           </div>
        </div>
    );
};

export default Contact;