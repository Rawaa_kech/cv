import React  from 'react';
import Navigation from '../components/Navigation';
const Home = () => {
    return (
        <div className="home">
            <Navigation/>
            <div className="homeContent">
                <div className="content">

                    <h1>Rawaa KECHICHE</h1>
                    <ul>
                        <li>
                    <i class="fas fa-file-signature"></i>
                    <span>Durée du Contrat d'apprentissage: 1 an</span>
                    </li>
                    <li>
                    <i className="fas fa-calendar"></i>
                    <span>Rythme: 3SEM.Entreprise/3SEM.Université</span>
                    </li>
                    <li>
                    <i class="far fa-clock"></i>
                    <span>Disponibilité:Septembre 2021</span>
                    </li>
                    </ul>
                    <div className="logo">
                      
                         <img src="./media/logo.png" alt="the university logo" width="140" height="90"/>
    
                        <img className="cfa" src="./media/cfa.png" alt="the cfa logo"width="130" />

                    </div>
                    
                    <div className="cv">
                        <a href ="./media/CV_kechiche.pdf" target="_blank">Télecharger CV</a> 

                    </div>

                </div>


            </div>

        </div>
    );
};

export default Home;