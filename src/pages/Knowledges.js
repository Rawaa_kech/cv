import React from 'react';
import Navigation from '../components/Navigation';
import Languages from '../components/khowledges/Languages';
import Formation from '../components/khowledges/Formation';
import OtherSkills from '../components/khowledges/OtherSkills';
import Hobbies from '../components/khowledges/Hobbies';


const Knowledges = () => {
    return (
      <div className="khowledges">
        <Navigation/>
          <div className="khowledgesContent">
        <Languages/>
        <Formation/>
        <OtherSkills/>
        <Hobbies/>
          </div>   
      </div>
    );
};

export default Knowledges;